<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{$title}}</title>
</head>
<body>
    <h1>Buat Account Baru</h1>

    <!-- form -->
    <form action="/welcome" method="POST">
        @csrf
        <h3>Sign Up Form</h3>
        <label for="firstname">First Name:</label> <br>
        <input type="text" placeholder="" value="" id="firstname" name="firstname" required> <br><br>
        <label for="lastname">Last Name:</label> <br>
        <input type="text" placeholder="" value="" id="lastname" name="lastname" required> <br><br>
        <label>Gender:</label> <br>
        <input type="radio" name="gender" value="0">Male <br>
        <input type="radio" name="gender" value="1">Female <br>
        <input type="radio" name="gender" value="2">Other <br><br>
        <label>Nationality:</label>
        <select>
            <option value="idn" name="nationality" value="0">Indonesian</option>
            <option value="sgp" name="nationality" value="1">Singaporean</option>
            <option value="mys" name="nationality" value="2">Malaysian</option>
            <option value="aus" name="nationality" value="3">Australian</option>
        </select> <br><br>
        <label>Language Spoken:</label> <br>
        <input type="checkbox" name="language_spoken" value="0">Bahasa Indonesia <br>
        <input type="checkbox" name="language_spoken" value="1">English <br>
        <input type="checkbox" name="language_spoken" value="2">Arabic <br>
        <input type="checkbox" name="language_spoken" value="3">Japanese <br><br>
        <label for="bio">Bio:</label> <br>
        <textarea id="bio" cols="45" rows="10" name="bio"></textarea> <br><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>