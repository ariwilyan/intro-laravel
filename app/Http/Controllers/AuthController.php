<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function register() {
        $title = 'Register';
        return view('register', compact('title'));
    }

    public function welcome(Request $request) {
        $title = "Welcome";
        $firstname = $request->firstname;
        $lastname = $request->lastname;
        return view('welcome', compact('title', 'firstname', 'lastname'));
    }
}
